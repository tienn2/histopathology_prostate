import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import pickle
#import cv2
import os
import time
import multiprocessing
import gc
import warnings

from collections import Counter
#from joblib import Parallel, delayed
#from shapely.geometry.polygon import Polygon
from matplotlib.cbook import boxplot_stats

from skimage import data
from skimage import morphology
from skimage import measure
from skimage.feature import greycomatrix, greycoprops, local_binary_pattern, peak_local_max, canny, hog
from skimage.io import imread, imshow
from skimage.color import rgb2hed, hed2rgb, rgb2gray, label2rgb, gray2rgb, rgb2lab, rgb2yuv
from skimage.util import crop, img_as_ubyte, img_as_float, invert, view_as_blocks
from skimage.filters import threshold_otsu, threshold_local, rank, sobel
from skimage.segmentation import clear_border
from skimage.exposure import adjust_gamma, rescale_intensity
from skimage.transform import match_histograms

from sklearn import svm
from sklearn.model_selection import GridSearchCV,StratifiedShuffleSplit,ShuffleSplit,cross_val_predict,cross_val_score,train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN
from sklearn.neighbors import KNeighborsClassifier
from sklearn.utils import shuffle
from sklearn.datasets import load_sample_image

from scipy.interpolate import UnivariateSpline
from scipy import ndimage as ndi
from scipy.spatial import Delaunay, ConvexHull

from functools import reduce

#############################################################################################################################

def factors(n):    
    return sorted(set(reduce(list.__add__, ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0))))


def crop_img(image_rgb, new_height, new_width) :
    height = image_rgb.shape[0]
    width = image_rgb.shape[1]
    top = int((height - new_height)/2)
    bottom = int((height + new_height)/2)
    left = int((width - new_width)/2)
    right = int((width + new_width)/2)
    
    if len(image_rgb.shape) == 2 :
        cropped_image = image_rgb[top:bottom, left:right]
    else :
        cropped_image = image_rgb[top:bottom, left:right, :]
    
    return cropped_image

def cv2_clipped_zoom(img, zoom_factor):
    """
    Center zoom in/out of the given image and returning an enlarged/shrinked view of 
    the image without changing dimensions
    Args:
        img : Image array
        zoom_factor : amount of zoom as a ratio (0 to Inf)
    """
    height, width = img.shape[:2] # It's also the final desired shape
    new_height, new_width = int(height * zoom_factor), int(width * zoom_factor)

    ### Crop only the part that will remain in the result (more efficient)
    # Centered bbox of the final desired size in resized (larger/smaller) image coordinates
    y1, x1 = max(0, new_height - height) // 2, max(0, new_width - width) // 2
    y2, x2 = y1 + height, x1 + width
    bbox = np.array([y1,x1,y2,x2])
    # Map back to original image coordinates
    bbox = (bbox / zoom_factor).astype(np.int)
    y1, x1, y2, x2 = bbox
    cropped_img = img[y1:y2, x1:x2]

    # Handle padding when downscaling
    resize_height, resize_width = min(new_height, height), min(new_width, width)
    pad_height1, pad_width1 = (height - resize_height) // 2, (width - resize_width) //2
    pad_height2, pad_width2 = (height - resize_height) - pad_height1, (width - resize_width) - pad_width1
    pad_spec = [(pad_height1, pad_height2), (pad_width1, pad_width2)] + [(0,0)] * (img.ndim - 2)

    result = cv2.resize(cropped_img, (resize_width, resize_height))
    result = np.pad(result, pad_spec, mode='constant')
    assert result.shape[0] == height and result.shape[1] == width
    
    return result


def recreate_image(codebook, labels, w, h):
    """
    Taken from : https://scikit-learn.org/stable/auto_examples/cluster/plot_color_quantization.html#
    Recreate the (compressed) image from the code book & labels.
    codebook : RGB values of kmeans clusters (i.e. : clusters_rgb = np.array([[0,255,0], [255,255,255], [255,0,0], [0,0,255]]) for 4 clusters)
    labels : labels predicted by kmeans clustering
    w, h : width and height of image respectively
    """
    
    d = codebook.shape[1]
    image = np.zeros((w, h, d))
    label_idx = 0
    for i in range(w):
        for j in range(h):
            image[i][j] = codebook[labels[label_idx]]
            label_idx += 1
    return image


def detect_nuclei(image_rgb, reference_image) :
    """
    Returns a mask with all epithelial nuclei detected and their contours.
    Variables :
    image_rgb : RGB image (3 channels), i.e. : image = imread("chat.jpg")
    reference_image : reference image with which image_rgb's histogram will be matched.
    """
    
    # Histogram matching with reference image and conversion to HED (hematoxylin, eosin, DAB) color space. Keep hematoxylin and eosin channels to produce masks later.

    matched = match_histograms(image_rgb, reference_image, multichannel=True)
    matched_hed = rgb2hed(matched)
    eosin = img_as_ubyte(rescale_intensity(matched_hed[:,:,1], out_range=(1,0))) # out_range=(1,0) pour avoir les noyaux à low pixel values et background à high pixel values (fonctionne mieux pour thresholding par la suite)
    hema = img_as_ubyte(rescale_intensity(matched_hed[:,:,0], out_range=(1,0)))
    
    # k-means clustering to get lumen pixels --> lumen mask
    
    n_colors = 3 #lumen, eosin, hematoxylin
    w, h, d = original_shape = tuple(matched.shape)
    assert d == 3
    image_array = np.reshape(matched, (w * h, d))
    image_array_sample = shuffle(image_array, random_state=0)[:1000]
    kmeans = KMeans(n_clusters=n_colors, random_state=0).fit(image_array_sample) # Fitting model on a small sub-sample of the data
    kmeans_labels_rgb = kmeans.predict(image_array) # Predicting color labels on the full image

    # Façon d'aller chercher le cluster du lumen trouvé par k-means (pcq l'ordre des clusters change à chaque fois que k-means est utilisé) : on vient garder le cluster qui est le plus proche du cluster blanc [255,255,255]
    
    white_cluster = [255, 255, 255]
    clusters_lumen = np.array([[0,0,0], [0,0,0], [0,0,0]])
    distances = [np.linalg.norm(kmeans.cluster_centers_.astype(int)[i] - white_cluster) for i in range(n_colors)]
    index = np.argmin(distances) # l'emplacement du cluster associé au lumen
    clusters_lumen[index] = white_cluster
    clustered_lumen = recreate_image(clusters_lumen, kmeans_labels_rgb, w, h)
    
    # Noise reduction : morphological operations and thresholding

    block_size = 75
    selem = morphology.disk(2)
    offset = 10
    
    thresh_eosin = threshold_otsu(eosin)
    binary_eosin = np.bitwise_not((eosin > thresh_eosin))
    
    local_thresh_hema = threshold_local(hema, block_size=block_size, offset=offset) # Local threshold pour les noyaux : permet de mieux discerner les noyaux individuellement (pas de gros blobs...)
    binary_hema = hema > local_thresh_hema

    closed_img = morphology.binary_closing(binary_hema, selem=selem)
    closed_opened_img = np.bitwise_not(morphology.binary_opening(closed_img, selem=selem))
    
    # Marker-based watershed segmentation : separate overlapping nuclei

    distance = ndi.distance_transform_edt(closed_opened_img)
    local_max = peak_local_max(distance, min_distance=8, indices=False, labels=closed_opened_img)
    markers = ndi.label(local_max)[0]
    labels_watershed = morphology.watershed(-distance, markers, mask=closed_opened_img, compactness=0.01, watershed_line=True)
    props = measure.regionprops(labels_watershed)
    list_areas = [props[i].area for i in range(len(props))]
    avg_area = np.mean(np.array(list_areas))
    labels_final = morphology.remove_small_objects(labels_watershed, min_size=0.20*avg_area)
    props_final = measure.regionprops(labels_final, intensity_image=hema, coordinates='rc')
    
    # Masks that will be used to extract neighborhood features

    nuclei_mask = (labels_final > 0).astype(np.int8)
    eosin_mask = morphology.binary_closing(np.bitwise_and(binary_eosin, np.invert(nuclei_mask)), selem=selem).astype(np.int8)
    lumen_mask = morphology.binary_closing(rgb2gray(clustered_lumen).astype(bool), selem=morphology.disk(1)).astype(np.int8)
    
    contours = measure.find_contours(nuclei_mask, level=np.mean(nuclei_mask)) #level=value along which to find contours in the array

    return contours, labels_final, props_final, nuclei_mask, eosin_mask, lumen_mask

def neighbor_features(props_final, eosin_mask, lumen_mask) :
    """
    Returns the amount of stroma and lumen pixels, number of neighbors and their distance around every nucleus detected in every image.
    
    Variables :
    props_final : list of RegionProps of an image for a single class (i.e. Gleason Score 3+3)
    eosin_mask, lumen_mask : binary masks of eosin stain and lumen
    """
    
    # Une map contenant l'emplacement des centroïdes.
    # Elle sera utilisée dans le comptage de noyaux voisins dans chaque itération de "patch"
    centroids = np.zeros((eosin_mask.shape[0], eosin_mask.shape[1]))

    # Remplissage de la map
    for nucleus in range(len(props_final)) :
        r, c = props_final[nucleus].centroid
        centroids[int(r), int(c)] = 1

    nuclei_morphology = np.empty((0,1))
    max_distances = np.empty((0,5)) # max distance voisin pour 3 steps, moyenne et var
    min_distances = np.empty((0,5)) # min distance voisin pour 3 steps, moyenne et var
    neighbors = np.empty((0,5)) # qté de voisins pour 3 steps, moyenne et var

    stroma_per_nucleus = np.empty((0,5)) # qté de stroma pour 3 steps, moyenne et var
    lumen_per_nucleus = np.empty((0,5)) # qté de lumière pour 3 steps, moyenne et var
    steps = [30, 50, 70]

    for nucleus in range(len(props_final)) :
        r, c = props_final[nucleus].centroid
        nuclei_morphology = np.vstack((nuclei_morphology, props_final[nucleus].convex_area))
        stroma = []
        lumen = []
        max_distances_temp = []
        min_distances_temp = []
        neighbors_temp = []

        for step in steps :
            if int(r)-step < 0 :
                row_start = 0
            else :
                row_start = int(r)-step

            if int(r)+step > eosin_mask.shape[0] :
                row_stop = eosin_mask.shape[0]
            else :
                row_stop = int(r)+step

            if int(c)-step < 0 :
                col_start = 0
            else :
                col_start = int(c)-step

            if int(c)+step > eosin_mask.shape[1] :
                col_stop = eosin_mask.shape[1]
            else :
                col_stop = int(c)+step

            range_row = slice(row_start, row_stop)
            range_col = slice(col_start, col_stop)
            slices = (range_row, range_col)

            indices = np.transpose(np.nonzero(centroids[slices])) # Retourne la position des centroïdes
            indices[:,0] = indices[:,0] + row_start # Conversion des indices des noyaux dans la région des slices en indices de l'image totale (pour utiliser avec centroïde d'intérêt)
            indices[:,1] = indices[:,1] + col_start
            position, = np.where(np.all(indices == [int(r),int(c)], axis=1))[0] # Retourne l'indice du centroïde d'intérêt dans la région slice
            indices = np.delete(indices, position, axis=0) # Supprime la coordonnée du centroïde et garde juste les centroïdes voisins

            if len(indices) == 0 : # Pas de centroïdes voisins
                distances = 0
                max_distance = 10000
                min_distance = 0
                num_neighbors = len(indices)
            else :
                distances = [np.linalg.norm(indices[i]-[int(r),int(c)]) for i in range(len(indices))] # Calcule la distance entre centroïde d'intérêt et ses voisins
                max_distance = np.max(distances)
                min_distance = np.min(distances)
                num_neighbors = len(indices)

            max_distances_temp.append(max_distance)
            min_distances_temp.append(min_distance)
            neighbors_temp.append(num_neighbors)

            stroma.append(np.sum(eosin_mask[slices]))
            lumen.append(np.sum(lumen_mask[slices]))

        max_distances_temp.extend([np.mean(max_distances_temp), np.var(max_distances_temp)])
        min_distances_temp.extend([np.mean(min_distances_temp), np.var(min_distances_temp)])
        neighbors_temp.extend([np.mean(neighbors_temp), np.var(neighbors_temp)])

        max_distances = np.vstack((max_distances, max_distances_temp))
        min_distances = np.vstack((min_distances, min_distances_temp))
        neighbors = np.vstack((neighbors, neighbors_temp))

        stroma.extend([np.mean(stroma), np.var(stroma)])
        lumen.extend([np.mean(lumen), np.var(lumen)])

        stroma_per_nucleus = np.vstack((stroma_per_nucleus, stroma))
        lumen_per_nucleus = np.vstack((lumen_per_nucleus, lumen))

    # total of 26 features
    neigh_features = np.hstack((stroma_per_nucleus, lumen_per_nucleus, max_distances, min_distances, neighbors, nuclei_morphology)) # shape : (# nuclei x 26 features)

    return neigh_features

def polygon_features(props) :
    """
    Fonction qui retourne l'aire des triangles données par la triangulation de Delaunay.
    La première partie de la fonction calcule la triangulation à partir des centroïdes des noyaux détectés et sauvegarde les coordonnées des centroïdes.
    La deuxième partie de la fonction vient chercher les coordonnées de chaque triangles et vient tracer un polygone (triangle dans ce cas-ci puisque chaque
    coordonnée est un ensemble de 3 points). Pour chaque polygone, l'aire est extraite.
    
    Variables :
    props : les RegionProps de toutes les images d'une classe (i.e. Gleason Score 3+3).
    """
    delaunay = []
    points = []
    
    for image in range(len(props)) :
        temp = []
        for nucleus in range(len(props[image])) :
            temp.append(props[image][nucleus].centroid)

        delaunay.append(Delaunay(temp))
        points.append(temp)

    #polygons = []
    polygon_areas = np.empty((0,1))
    polygon_std = np.empty((0,1))
    for i in range(len(points)) :
        #temp_polygons = [] # Réinitialise la liste des polygones pour la prochaine image
        temp_areas = []
        coord_groups = []

        for index in delaunay[i].simplices : # Itération sur les sommets pour une image i (simplices : indices of the points forming the simplices in the triangulation)
            coord_groups.append(delaunay[i].points[index]) # Chercher les coordonnées des sommets des triangles de l'image i

        for coord in coord_groups : # Itération sur coordonnées de chaque triangle
            polygon = Polygon(coord)
            #temp_polygons.append(polygon)
            temp_areas.append(polygon.area)

        #polygons.append(temp_polygons) # Donnera une liste de polygones (shape = # d'images)
        polygon_areas = np.vstack((polygon_areas, np.mean(temp_areas)))
        polygon_std = np.vstack((polygon_std, np.std(temp_areas)))
        
    polygon_features = np.hstack((polygon_areas, polygon_std)) # final shape : (# d'images x 2 features)
        
    
    return polygon_features

def get_densities_patches_feature(props_final, nuclei_mask, eosin_mask, lumen_mask):
    """
    This function splits an image in different patches. It then creates a map with all the centroids
    from the RegionProps given by the function detect_nuclei(...). In each image patch, the nuclei density
    is calculated by summing the centroids (value 1) in the corresponding patched centroid map. With the same
    procedure, the hematoxylin (nuclei mask), lumen and eosin (stroma mask) densities are also calculated for every patch.

    Variables :
    props_final, lumen_mask and eosin_mask : the returned variables from detect_nuclei(...) useful here.
    """

    centroids = np.zeros((eosin_mask.shape[0], eosin_mask.shape[1]))
    for i in range(len(props_final)):
        r, c = props_final[i].centroid
        centroids[int(r), int(c)] = 1

    height, width = centroids.shape[0], centroids.shape[1] # rows and columns
    patch_height, patch_width = np.max([x for x in factors(height) if 200 <= x <= 350]), np.max([x for x in factors(width) if 200 <= x <= 350])
    patch_shape = (patch_height, patch_width) # shape = (# patch rows, # patch columns, # row pixels/patch, # column pixels/patch)
    patch_area = patch_height*patch_width

    patches_centroids = view_as_blocks(centroids, patch_shape)
    patches_nuclei = view_as_blocks(nuclei_mask, patch_shape)
    patches_lumen = view_as_blocks(lumen_mask, patch_shape)
    patches_eosin = view_as_blocks(eosin_mask, patch_shape)

    num_nuclei_densities = []
    nuclei_densities = []
    lumen_densities = []
    eosin_densities = []

    for i in range(patches_centroids.shape[0]) :
        for j in range(patches_centroids.shape[1]) :
            lumen_percentage = np.sum(patches_lumen[i][j])/patch_area # remove patches that have too much lumen (background biopsy)
            if lumen_percentage <= 0.4 :
                num_nuclei_densities.append(np.sum(patches_centroids[i][j])/patch_area)
                nuclei_densities.append(np.sum(patches_nuclei[i][j])/patch_area)
                lumen_densities.append(np.sum(patches_lumen[i][j])/patch_area)
                eosin_densities.append(np.sum(patches_eosin[i][j])/patch_area)

    num_nuclei_densities_stats = np.array([np.amin(num_nuclei_densities), np.amax(num_nuclei_densities), np.mean(num_nuclei_densities), np.var(num_nuclei_densities)])
    nuclei_densities_stats = np.array([np.amin(nuclei_densities), np.amax(nuclei_densities), np.mean(nuclei_densities), np.var(nuclei_densities)])
    lumen_densities_stats = np.array([np.amin(lumen_densities), np.amax(lumen_densities), np.mean(lumen_densities), np.var(lumen_densities)])
    eosin_densities_stats = np.array([np.amin(eosin_densities), np.amax(eosin_densities), np.mean(eosin_densities), np.var(eosin_densities)])
    # 16 features total
    densities_features = np.hstack((num_nuclei_densities_stats, nuclei_densities_stats, lumen_densities_stats, eosin_densities_stats))
    
    return densities_features

def get_densities_wholeimage_feature(nuclei_mask, stroma_mask) :
	nuclei_area = np.sum(nuclei_mask)
	tissue_area = np.sum(stroma_mask)
	densities_wholeimage = nuclei_area/tissue_area

	return densities_wholeimage