# Description of the project on histopathological prostate images.

As of September 8th, all biopsy H&E images are from the ISUP database, all ranging from gleason score (GS) 3+3 to 5+5.<br/>
- Classification task attempt of Gleason score.
- Correlation of nuclei densities with biochemical recurrence (not yet tried...)

## First task attempt of the project : classification of grades.

The steps to perform this task :
- Image normalization is applied with a good H&E reference image. This ensures that poorly stained images can be workable with.
- Stain deconvolution : RGB colorspace to H&E-DAB colorspace. 
- From these staining channels, lumen, stroma and nuclei masks are extracted.
- With the extracted masks, features are calculated.
- Neighborhood features : for every nuclei detected, we define a patch region centered on said nuclei and calculate amount of pixels related to stroma and lumen, calculate number of nuclei neighbors and their distance to nuclei of interest. Repeat for 3 different patch sizes.
- Density patches features : divide the whole image in different patches. For every patch, calculate the ratio nuclei\_pixels/stroma\_pixels and num\_nuclei/area\_patch.
- Density whole image features : calculate nuclei\_mask/stroma_\mask ratio for every image.

A total of 43 features.

Feature importance is calculated with several methods : ExtraTrees and RandomForest classifiers and univariate feature selection reveal that density-related features are best to classify between gleason grades.
Precisely : 
- 1st : neighborhood feature : mean nuclei\_densities
- 2nd : neighborhood feature : max nuclei\_densities
- 3rd : neighborhood feature : minimum distance at patch1
- 4th : neighborhood feature : mean mininum distances
- 5th : density whole image feature

Classes are pretty unbalanced :
- GS 3+3 : 57
- GS 3+4 : 62
- GS 4+3 : 9
- GS 4+4 : 16
- GS 9/10 : 19

### Classification results : 
- Best kernel : Linear SVM
- Best hyperparameters : C=1, gamma=NA
- Cross-validation score (cv=10) : 0.45
- Testing score (20% holdout) : 0.58
